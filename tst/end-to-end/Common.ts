import { expect } from 'chai'

import { compilePattern } from '../../src/PatternCompiler'
import { PatternBuilder } from '../../src/PatternBuilder'

// Based on: https://fireship.io/snippets/crontab-crash-course/
describe('Common', () => {

	it('every minute', () => {

		// Assign
		const pattern: PatternBuilder = ({ x }) => [x, x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('* * * * *')
	})

	it('every 15 minutes', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, step }) => [step(x, 15), x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('*/15 * * * *')
	})

	it('every day at 10:00 and 22:00', () => {

		// Assign
		const pattern: PatternBuilder = ({ x }) => [0, [10, 22], x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('0 10,22 * * *')
	})

	it('every Monday and Wednesday at 20:00', () => {

		// Assign
		const pattern: PatternBuilder = ({ x }) => [0, 20, x, x, [1, 3]]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('0 20 * * 1,3')
	})

	it('every 5 Minutes, between 09:00 and 17:00, but only on weekdays', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, step, range }) => [step(x, 5), range(9, 17), x, x, range(1, 5)]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('*/5 9-17 * * 1-5')
	})
})
