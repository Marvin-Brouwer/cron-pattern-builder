import { expect } from 'chai'

import { compilePattern } from '../../src/PatternCompiler'
import { PatternBuilder } from '../../src/PatternBuilder'

describe('Simple', () => {

	it('wildcards', () => {

		// Assign
		const pattern: PatternBuilder = ({ x }) => [2, x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('2 * * * *')
	})

	it('step', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, step }) => [step(x, 2), x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('*/2 * * * *')
	})

	it('range', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, range }) => [range(1, 2), x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('1-2 * * * *')
	})

	it('array', () => {

		// Assign
		const pattern: PatternBuilder = ({ x }) => [[1, 2, 3, 4], x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('1,2,3,4 * * * *')
	})
})
