import { expect } from 'chai'

import { compilePattern } from '../../src/PatternCompiler'
import { PatternBuilder } from '../../src/PatternBuilder'

describe('Nested', () => {

	it('array with step', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, step }) => [[1, step(1, 5)], x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('1,1/5 * * * *')
	})

	it('step with range', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, step, range }) => [step(range(1, 3), 5), x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('1-3/5 * * * *')
	})

	it('array with step with range', () => {

		// Assign
		const pattern: PatternBuilder = ({ x, range, step }) => [[range(1, 2), step(range(1, 3), 5), 8], x, x, x, x]

		// Act
		const result = compilePattern(pattern)

		// Assert
		expect(result).to.be.equal('1-2,1-3/5,8 * * * *')
	})
})
