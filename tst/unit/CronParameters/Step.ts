import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { isStep } from '../../../src/CronParameters/Step'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { cronAny } from '../../../src/CronParameters/Any'

// tslint:disable:no-unused-expression
describe('Step', () => {

	it('isStep correctPattern returnsTrue', () => {

		// Assign
		const pattern: CronParameter = { numerator: 0, denominator: 1 }

		// Act
		const result = isStep(pattern)

		// Assert
		expect(result).to.be.true
	})

	it('isStep malformedPattern returnsFalse', () => {

		// Assign
		const pattern1: CronParameter = { numerator: 0 } as any
		const pattern2: CronParameter = { denominator: 1 } as any

		// Act
		const result1 = isStep(pattern1)
		const result2 = isStep(pattern2)

		// Assert
		expect(result1).to.be.false
		expect(result2).to.be.false
	})

	it('isStep incorrectPattern returnsFalse', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = isStep(pattern)

		// Assert
		expect(result).to.be.false
	})

	it('isStep noPattern throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => isStep(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})
})
