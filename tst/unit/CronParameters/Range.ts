import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { isRange } from '../../../src/CronParameters/Range'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { cronAny } from '../../../src/CronParameters/Any'

// tslint:disable:no-unused-expression
describe('Range', () => {

	it('isRange correctPattern returnsTrue', () => {

		// Assign
		const pattern: CronParameter = { range: [0, 2] }

		// Act
		const result = isRange(pattern)

		// Assert
		expect(result).to.be.true
	})

	it('isRange incorrectPattern returnsFalse', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = isRange(pattern)

		// Assert
		expect(result).to.be.false
	})

	it('isRange noPattern throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => isRange(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})
})
