import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { isNumber } from '../../../src/CronParameters/Number'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { cronAny } from '../../../src/CronParameters/Any'

// tslint:disable:no-unused-expression
describe('Number', () => {

	it('isNumber correctPattern returnsTrue', () => {

		// Assign
		const pattern: CronParameter = 4

		// Act
		const result = isNumber(pattern)

		// Assert
		expect(result).to.be.true
	})

	it('isNumber incorrectPattern returnsFalse', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = isNumber(pattern)

		// Assert
		expect(result).to.be.false
	})

	it('isNumber noPattern throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => isNumber(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})
})
