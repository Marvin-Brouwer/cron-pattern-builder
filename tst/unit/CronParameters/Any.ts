import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { cronAny, isAny } from '../../../src/CronParameters/Any'
import { NoParameterError } from '../../../src/Errors/NoParameterError'

// tslint:disable:no-unused-expression
describe('Any', () => {

	it('isAny correctPattern returnsTrue', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = isAny(pattern)

		// Assert
		expect(result).to.be.true
	})

	it('isAny incorrectPattern returnsFalse', () => {

		// Assign
		const pattern: CronParameter = {} as any

		// Act
		const result = isAny(pattern)

		// Assert
		expect(result).to.be.false
	})

	it('isAny noPattern throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => isAny(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})
})
