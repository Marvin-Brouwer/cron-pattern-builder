import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { isRangeArray } from '../../../src/CronParameters/RangeArray'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { cronAny } from '../../../src/CronParameters/Any'

// tslint:disable:no-unused-expression
describe('RangeArray', () => {

	it('isRangeArray correctPattern returnsTrue', () => {

		// Assign
		const pattern: CronParameter = [0, 1]

		// Act
		const result = isRangeArray(pattern)

		// Assert
		expect(result).to.be.true
	})

	it('isRangeArray incorrectPattern returnsFalse', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = isRangeArray(pattern)

		// Assert
		expect(result).to.be.false
	})

	it('isRangeArray noPattern throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => isRangeArray(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})
})
