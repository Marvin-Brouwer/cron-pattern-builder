import { expect } from 'chai'

import { number } from '../../../src/PatternCompiler'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { CronPatternError } from '../../../src/Errors/CronPatternError'

// tslint:disable:no-unused-expression
describe('PatternCompiler', () => {

	it('compileNumber noPattern throws', () => {

		// Assign
		const pattern = undefined as any

		// Act
		const result = () => number(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})

	it('compileNumber isNegatvie throws', () => {

		// Assign
		const pattern = -1

		// Act
		const result = () => number(pattern)

		// Assert
		expect(result).to.throw(CronPatternError)
	})

	it('compileNumber correctPattern returnsString', () => {

		// Assign
		const pattern = 4

		// Act
		const result = number(pattern)

		// Assert
		expect(result).to.be.equal('4')
	})
})
