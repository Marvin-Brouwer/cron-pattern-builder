import { expect } from 'chai'

import { array } from '../../../src/PatternCompiler'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { CronPatternError } from '../../../src/Errors/CronPatternError'

// tslint:disable:no-unused-expression
describe('PatternCompiler', () => {

	it('compileArray emptyArguments throws', () => {

		// Assign
		const items = undefined as any

		// Act
		const result = () => array(items)

		// Assert
		expect(result).to.throw(NoParameterError)
	})

	it('compileArray zeroLength throws', () => {

		// Assign
		const items = []

		// Act
		const result = () => array(items)

		// Assert
		expect(result).to.throw(CronPatternError)
	})

	it('compileArray correctPattern returnsString', () => {

		// Assign
		const items = ['1', '2', '*/2']

		// Act
		const result = array(items)

		// Assert
		expect(result).to.be.equal('1,2,*/2')
	})
})
