import { expect } from 'chai'

import { CronParameter } from '../../../src/CronParameters/CronParameter'
import { compile } from '../../../src/PatternCompiler'
import { cronAny } from '../../../src/CronParameters/Any'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { CronPatternError } from '../../../src/Errors/CronPatternError'

// tslint:disable:no-unused-expression
describe('PatternCompiler', () => {

	it('compile noParam throws', () => {

		// Assign
		const pattern: CronParameter = undefined as any

		// Act
		const result = () => compile(pattern)

		// Assert
		expect(result).to.throw(NoParameterError)
	})

	it('compile any returnsString', () => {

		// Assign
		const pattern: CronParameter = cronAny

		// Act
		const result = compile(pattern)

		// Assert
		expect(result).to.be.equal('*')
	})

	it('compile number returnsString', () => {

		// Assign
		const pattern: CronParameter = 4

		// Act
		const result = compile(pattern)

		// Assert
		expect(result).to.be.equal('4')
	})

	it('compile range returnsString', () => {

		// Assign
		const pattern: CronParameter = { range: [1, 2] }

		// Act
		const result = compile(pattern)

		// Assert
		expect(result).to.be.equal('1-2')
	})

	it('compile step returnsString', () => {

		// Assign
		const pattern: CronParameter = { numerator: 1, denominator: 2 }

		// Act
		const result = compile(pattern)

		// Assert
		expect(result).to.be.equal('1/2')
	})

	it('compile rangeArray returnsString', () => {

		// Assign
		const pattern: CronParameter = [1, 2, 3]

		// Act
		const result = compile(pattern)

		// Assert
		expect(result).to.be.equal('1,2,3')
	})

	it('compile unknownParameter throws', () => {

		// Assign
		const pattern: CronParameter = {} as any

		// Act
		const result = () => compile(pattern)

		// Assert
		expect(result).to.throw(CronPatternError)
	})
})
