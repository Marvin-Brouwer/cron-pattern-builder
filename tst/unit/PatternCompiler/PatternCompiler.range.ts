import { expect } from 'chai'

import { range } from '../../../src/PatternCompiler'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { CronPatternError } from '../../../src/Errors/CronPatternError'

// tslint:disable:no-unused-expression
describe('PatternCompiler', () => {

	it('compileRange emptyArguments throws', () => {

		// Assign
		const from = 0
		const to = 4

		// Act
		const result1 = () => range(undefined as any, to)
		const result2 = () => range(from, undefined as any)

		// Assert
		expect(result1).to.throw(NoParameterError)
		expect(result2).to.throw(NoParameterError)
	})

	it('compileRange negativeArguments throws', () => {
		// Assign
		const from = 0
		const to = 4

		// Act
		const result1 = () => range(-1, to)
		const result2 = () => range(from, -1)

		// Assert
		expect(result1).to.throw(CronPatternError)
		expect(result2).to.throw(CronPatternError)
	})

	it('compileRange toLessThanFrom throws', () => {

		// Assign
		const from = 2
		const to = 1

		// Act
		const result = () => range(from, to)

		// Assert
		expect(result).to.throw(CronPatternError)
	})

	it('compileRange correctPattern returnsString', () => {

		// Assign
		const from = 0
		const to = 4

		// Act
		const result = range(from, to)

		// Assert
		expect(result).to.be.equal('0-4')
	})
})
