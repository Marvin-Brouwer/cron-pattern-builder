import { expect } from 'chai'

import { step } from '../../../src/PatternCompiler'
import { NoParameterError } from '../../../src/Errors/NoParameterError'
import { CronPatternError } from '../../../src/Errors/CronPatternError'

// tslint:disable:no-unused-expression
describe('PatternCompiler', () => {

	it('compileStep emptyArguments throws', () => {

		// Assign
		const from = '0'
		const to = 4

		// Act
		const result1 = () => step(undefined as any, to)
		const result2 = () => step(from, undefined as any)

		// Assert
		expect(result1).to.throw(NoParameterError)
		expect(result2).to.throw(NoParameterError)
	})

	it('compileStep negativeArguments throws', () => {
		// Assign
		const from = '0'

		// Act
		const result = () => step(from, -1)

		// Assert
		expect(result).to.throw(CronPatternError)
	})

	it('compileStep correctPattern returnsString', () => {

		// Assign
		const from = '0'
		const to = 4

		// Act
		const result = step(from, to)

		// Assert
		expect(result).to.be.equal('0/4')
	})
})
