[//]: # (Header)

<header align="center">
	<a  href="https://gitlab.com/Marvin-Brouwer/cron-pattern-builder#readme" 
		align="center" >
		<p align="center">
			<img  width="250" height="250" 
				align="center" alt="Cron pattern builder" title="Cron pattern builder" 
				src="https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/raw/master/resource/banner.png?inline=false" />
		</p>
	</a>
</header>

[![License][license-image]][license-url]  
[![NPM version][npm-version-image]][npm-package-url] <span>&ensp;</span>
[![Downloads][npm-downloads-image]][npm-package-url]  
<sub>(Gitlab badges are not showing if you're not a repo team-member)</sub>  
[![Build Status][build-image]][build-url] <span>&ensp;</span>
[![Coverage][coverage-image]][build-url]  
  
[//]: # (Documentation)
  
---  
  
## Install  
  
```plaintext
yarn add cron-pattern-builder
pnpm add cron-pattern-builder
   npm i cron-pattern-builder
```
  
## Summary  
  
A typed way to build [cron][crontab-url] expression patterns.  
This library provides Javascript/Typescript developers with an SDK that generates [cron][crontab-url] expressions.  
  
It will generate a valid [cron][crontab-url] expression pattern based on syntax but it won't validate whether the ranges are correct(yet).  
  
### Usage  
[common-patterns-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/blob/master/tst/end-to-end/Common.ts
[end-to-end-tests-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/blob/master/tst/end-to-end
  
To generate an expression create a function that is typed as `PatternBuilder`, and pass that to the `compilePattern` function. 
The `PatternBuilder` type will provide you with some useful type information when using Typescript.  
  
The `PatternBuilder` type will provide you with these functions:  
- **x**: This is the cron wildcard (`*`)
- **step**: This is the cron step pattern (`n/n`)
- **range**: This is the cron range pattern (`n-n`)  
  
These functions are nestable according to the [crontab][crontab-url] specification.  
To represent a [cron][crontab-url] expression you will need to return an array of exactly 5 items, these items are only allowed to be any of these types:  
- `number`
- Any of the functions exposed by the `PatternBuilder`
- A single dimension array of either `number` or any of the functions exposed by the `PatternBuilder`
  This will indicate a cron number range (`n,n,n,...`)  
  
The `PatternBuilder` can be used directly in the `compilePattern` function or in a variable, storing the `PatternBuilder`. 
This might be useful to store a few builders for a tool that generates [cron][crontab-url] expressions dynamically.  
  
<sub>Crontab.ts</sub> 
```typescript
import { compilePattern, PatternBuilder } from 'cron-pattern-builder'

// Call directly
const everyMinutePattern = compilePattern(({ x }) => [x, x, x, x, x])

// Call from a variable
const somePatternBuilder: PatternBuilder = ({ x, step }) => [step(x, 5), x, x, x, x]
const everyFiveMinutesPattern = compilePattern(somePatternBuilder)
```  
For some more interesting examples see: [Common patterns][common-patterns-url] or any other test in the [end-to-end tests folder][end-to-end-tests-url].
  
## Remarks  
**This** library uses a devDependency in alpha stage called `@delta-framework/compiler`  
Even though the delta-framework is in alpha state, this package is only used for the deploy automation.  
It's not a part of the package code.  
  
**The** TsDoc might be verbose but the goal is to have inline Cron documentation.  
Because of that, all exported members are attributed with TsDoc.  
  
**Sorry** for the ugly icon art.  
  
**For** some additional help with cron checkout:
- [Crontab guru](https://crontab.guru/)
- [Original crontab specification][crontab-url]
  
[//]: # (Additional documentation)  
<span>&nbsp;</span>  
  
## [Contributing][contributing-url]  
[contributing-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/blob/master/Contributing.md
  
See the [Contribution guide][contributing-url] for help about contributing to this project.  
  
## [Changelog][changelog-url]  
[changelog-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/blob/master/Changelog.md
  
See the [Changelog][changelog-url] to see the change history.  
  
[//]: # (Labels)

[gitlab-url]: https://gitlab.com/

[crontab-url]: http://crontab.org/

[npm-url]: https://www.npmjs.com
[npm-package-url]: https://www.npmjs.com/package/cron-pattern-builder
[npm-version-image]: https://img.shields.io/npm/v/cron-pattern-builder.svg?style=flat-square
[npm-downloads-image]: https://img.shields.io/npm/dm/cron-pattern-builder.svg?style=flat-square

[license-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/blob/master/License.md#blob-content-holder
[license-image]: https://img.shields.io/badge/license-Apache--2.0-blue.svg?style=flat-square
[build-url]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/pipelines
[build-image]: https://gitlab.com/Marvin-Brouwer/cron-pattern-builder/badges/master/pipeline.svg?style=flat-square
[coverage-image]: https://gitlab.com//Marvin-Brouwer/cron-pattern-builder/badges/master/coverage.svg?style=flat-square