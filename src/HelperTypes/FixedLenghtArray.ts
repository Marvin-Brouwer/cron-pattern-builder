type ArrayData<T> = T extends Array<infer U> ? U : never
type ArrayLengthMutationKeys = 'splice' | 'push' | 'pop' | 'shift' | 'unshift' | number

/**
 * Type to fix the array length in the type checker.
 * It's basically just a @see REadonlyArray but with a fixed length based on the type tuple.
 */
export type FixedLengthArray<T extends Array<any>> =
	Pick<T, Exclude<keyof T, ArrayLengthMutationKeys>>
	& { [Symbol.iterator]: () => IterableIterator<Readonly<ArrayData<T>>> }