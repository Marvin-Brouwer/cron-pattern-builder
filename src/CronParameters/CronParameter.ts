import { CronRange } from './Range'
import { CronStep } from './Step'
import { CronRangeArray } from './RangeArray'
import { CronAny } from './Any'

/**
 * A value cron patterns understand
 * @see http://crontab.org/
 */
export type CronParameter = number | CronStep | CronRange | CronRangeArray | CronAny