import { CronParameter } from './CronParameter'
import { NoParameterError } from '../Errors/NoParameterError'

/** Symbol to depict the cron * token */
export const cronAny = Symbol.for('cron-pattern-builder: cronAny[*]')

/**
 * The asterisk ("\*") is a wildcard to depict that this column is not bound to a specific time or range
 */
export type CronAny = typeof cronAny & {}

/**
 * Check if the parameter is a @see CronAny
 * @param parameter Parameter to validate being of @see CronAny
 */
export const isAny = (parameter: CronParameter): parameter is CronAny => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	return parameter === cronAny
}