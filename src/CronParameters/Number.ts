import { CronParameter } from './CronParameter'
import { NoParameterError } from '../Errors/NoParameterError'

/**
 * The number is a regular positive integer to inicate the amount of repetition in the column
 */
export type CronNumber = number & {}

/**
 * Check if the parameter is a @see CronNumber
 * @param parameter Parameter to validate being of @see CronNumber
 */
export const isNumber = (parameter: CronParameter): parameter is CronNumber => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	return Number.isInteger(parameter as any)
}