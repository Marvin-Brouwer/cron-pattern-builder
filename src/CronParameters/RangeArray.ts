import { CronRange } from './Range'
import { CronStep } from './Step'
import { NoParameterError } from '../Errors/NoParameterError'
import { CronParameter } from './CronParameter'

/**
 * The rangeArray ("[number|step|range],number|step|range, ...") is a way to write a list of matching values.
 * It requires at least one item.
 *
 * In this library this is done by nesting an @see Array in the @see CronPattern
 */
export type CronRangeArray = ReadonlyArray<number | CronStep | CronRange> & {
	[0]: number | CronStep | CronRange
}

/**
 * Check if the parameter is a @see CronRangeArray
 * @param parameter Parameter to validate being of @see CronRangeArray
 */
export const isRangeArray = (parameter: CronParameter): parameter is CronRangeArray => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	return Array.isArray(parameter)
}