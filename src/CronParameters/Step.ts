import { CronRange } from './Range'
import { CronAny } from './Any'
import { CronParameter } from './CronParameter'
import { NoParameterError } from '../Errors/NoParameterError'

/**
 * The step ("[number|step|*]\/[number]") is a way to depict parts of a time unit.
 */
export type CronStep = {
	numerator: number | CronRange | CronAny
	denominator: number
}

/**
 * Check if the parameter is a @see CronStep
 * @param parameter Parameter to validate being of @see CronStep
 */
export const isStep = (parameter: CronParameter): parameter is CronStep => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	const step = parameter as CronStep

	if (step.numerator === null || step.numerator === undefined) return false
	if (step.denominator === null || step.denominator === undefined) return false

	return true
}