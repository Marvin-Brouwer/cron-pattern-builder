import { FixedLengthArray } from '../HelperTypes/FixedLenghtArray'
import { CronParameter } from './CronParameter'
import { NoParameterError } from '../Errors/NoParameterError'

/**
 * The range ("[number]-[number]") is a shorthand for writing a comma separted list of numbers.
 */
export type CronRange = {
	range: FixedLengthArray<[number, number]>
}

/**
 * Check if the parameter is a @see CronRange
 * @param parameter Parameter to validate being of @see CronRange
 */
export const isRange = (parameter: CronParameter): parameter is CronRange => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	const step = parameter as CronRange
	return step.range !== null && step.range !== undefined
}