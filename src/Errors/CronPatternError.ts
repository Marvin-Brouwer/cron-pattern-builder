/**
 * Error depicting the requested pattern was incorrect
 */
export class CronPatternError extends Error { }