/**
 * Error flagging not all parameters were passed correctly to the current function
 */
export class NoParameterError extends ReferenceError {
	constructor(parameterName: string) {
		super(`Parameter '${parameterName}' was null or undefined`)
	}
}