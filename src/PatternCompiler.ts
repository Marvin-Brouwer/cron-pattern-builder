import { NoParameterError } from './Errors/NoParameterError'
import { rangeArraySeparator, rangeSeparator, stepSeparator, anyToken, partSeparator } from './Constants/StringConstants'
import { CronParameter } from './CronParameters/CronParameter'
import { PatternBuilder } from './PatternBuilder'
import { CronPatternError } from './Errors/CronPatternError'
import { cronParameters } from './Constants/CompilerConstants'
import { isAny } from './CronParameters/Any'
import { isNumber } from './CronParameters/Number'
import { isStep } from './CronParameters/Step'
import { isRange } from './CronParameters/Range'
import { isRangeArray } from './CronParameters/RangeArray'

export const number = (item: number) => {

	if (item === null || item === undefined) throw new NoParameterError('items')
	if (item < 0) throw new CronPatternError('A number value should be 0 or larger')

	return item.toString()
}
export const array = (items: Array<string>) => {

	if (items === null || items === undefined) throw new NoParameterError('items')
	if (items.length === 0) throw new CronPatternError('A rangeArray should have at least one item')

	return items.join(rangeArraySeparator)
}
export const range = (from: number, to: number) => {

	if (from === null || from === undefined) throw new NoParameterError('from')
	if (to === null || to === undefined) throw new NoParameterError('to')
	if (from < 0) throw new CronPatternError(`The "from" has to be 0 or larger in a range, exept got '${from}'`)
	if (to < 0) throw new CronPatternError(`The "to" has to be 1 or larger in a range, exept got '${from}'`)

	const rangeString = `${from}${rangeSeparator}${to}`
	if (to <= from) throw new CronPatternError('The "to" has to be larger than the "from" in a range, ' +
		`except got '${rangeString}'`)

	return rangeString
}
export const step = (numerator: string, denominator: number) => {

	if (numerator === null || numerator === undefined) throw new NoParameterError('numerator')
	if (denominator === null || denominator === undefined) throw new NoParameterError('denominator')
	if (denominator < 0) throw new CronPatternError(`The "denominator" has to be 0 or larger in a step, ` +
		`exept got '${denominator}'`)

	return `${numerator}${stepSeparator}${denominator}`
}

export const compile = (parameter: CronParameter): string => {

	if (parameter === null || parameter === undefined) throw new NoParameterError('parameter')

	if (isAny(parameter)) return anyToken
	if (isNumber(parameter)) return number(parameter)
	if (isRange(parameter))
		return (range(parameter.range[0], parameter.range[1]))
	if (isStep(parameter))
		return step(compile(parameter.numerator), parameter.denominator)
	if (isRangeArray(parameter))
		return array(parameter.map(compile))

	// Normal usage of api should never get here
	throw new CronPatternError('An unrecognized patterntype was used')
}

/**
 * Compile a @see PatternBuilder expression to a cron-valid string.
 * @param builder An expression that returns a valid @see CronPattern.
 */
/* Not unit-testable, this will be covered by integration tests */
/* istanbul ignore next */
export const compilePattern = (builder: PatternBuilder): string => {

	if (!builder) throw new NoParameterError('builder')

	const cronPattern = builder(cronParameters)

	if (!cronPattern)
		throw new CronPatternError("Patternbuilder didn't return a pattern")
	if (cronPattern.length !== 5)
		throw new CronPatternError(`The cronpattern length is expexted to be '6' but got '${cronPattern.length}'`)

	return cronPattern
		.map(compile)
		.join(partSeparator)
}