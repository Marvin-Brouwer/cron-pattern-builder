import { CronAny } from './CronParameters/Any'
import { CronRange } from './CronParameters/Range'
import { CronStep } from './CronParameters/Step'
import { CronPattern } from './CronPattern'

/**
 * Values cron patterns understand, derefence to get documenation
 *
 * @see http://crontab.org/
 */
export type CronParameters = {
	/**
	 * The asterisk ("\*") is a wildcard to depict that this column is not bound to a specific time or range
	 *
	 * @see CronAny
	 */
	x: CronAny,
	/**
	 * The range ("[number]-[number]") is a shorthand for writing a comma separted list of numbers.
	 *
	 * For example: "1-5" is the same as "1,2,3,4,5"
	 *
	 * @see CronRange
	 */
	range: (from: number, to: number) => CronRange,
	/**
	 * The step ("[number|step|*]\/[number]") is a way to depict parts of a time unit.
	 *
	 * For example: "*\/5" means "every five of this columns unit".
	 *
	 * @see CronStep
	 */
	step: (numerator: CronStep['numerator'], denominator: CronStep['denominator']) => CronStep
}

/**
 * Function for building Cron patterns with typed support, @see CronPattern.
 *
 * The pattern array must be 5 "columns" long and consists out of:
 * - Minutes [0-59]
 * - Hours [0-23]
 * - Day of month [1-31]
 * - Month [1-12]
 * - Day of week [0-6]
 *
 * For each of the columns you can use these values for generating expressions:
 * - any: This is a value that indicates a wildcard "\*"
 * - step: This method creates a step expression "[number|range]\/[number|*]"
 * - range: This function creates a range expression "[number]-[number]"
 *
 * The functions are required to adhere to the rules of the column, this is not typechecked.
 *
 * For every column an @see Array is allowed to depict a range of number "[number|step|range],[number|step|range], ...".
 * It requires at least one element
 *
 * @see http://crontab.org/
 * @see https://crontab.guru
 */
export type PatternBuilder = (patterns: CronParameters) => CronPattern
