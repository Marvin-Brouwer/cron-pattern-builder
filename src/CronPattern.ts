import { FixedLengthArray } from './HelperTypes/FixedLenghtArray'
import { CronParameter } from './CronParameters/CronParameter'

/**
 * A CRON expression is a string comprising five or six fields separated by white space that represents a set of times,
 * normally as a schedule to execute some routine.
 *
 * The pattern array must be 5 "columns" long and consists out of:
 * - Minutes [0-59]
 * - Hours [0-23]
 * - Day of month [1-31]
 * - Month [1-12]
 * - Day of week [0-6]
 *
 * For each of the columns parts, ranges and wildcards(*) are allowed.
 *
 * @see http://crontab.org/
 * @see https://crontab.guru
 */
export type CronPattern = FixedLengthArray<[
	CronParameter,
	CronParameter,
	CronParameter,
	CronParameter,
	CronParameter
]>
