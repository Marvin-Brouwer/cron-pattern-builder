import { cronAny } from '../CronParameters/Any'
import { CronParameters } from '../PatternBuilder'

/**
 * Default set of parameters used to supply the @see PatternBuilder with known @see CronParameters
 */
export const cronParameters: CronParameters = {
	x: cronAny,
	step: (numerator, denominator) => ({ numerator, denominator }),
	range: (...cronRanges) => ({ range: cronRanges })
}