/*
 * Constant string values used by the PatternBuilder
 */

export const anyToken = /*				*/ String.fromCharCode(42) // '*'
export const rangeArraySeparator = /*	*/ String.fromCharCode(44) // ','
export const rangeSeparator = /*		*/ String.fromCharCode(45) // '-'
export const stepSeparator = /*			*/ String.fromCharCode(47) // '/'
export const partSeparator = /*			*/ String.fromCharCode(32) // ' '